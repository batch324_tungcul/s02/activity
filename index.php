<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S02: Selection Control Structures and Array Manipulation</title>
</head>
<body>

	<h3>Divisibles by 5</h3>
	<p><?php printNumbersDivisibleBy5(); ?></p>

	<p><?php array_push($students, 'John Smith'); ?></p>
	<p><?php print_r($students); ?></p>

	<p><?php echo count($students); ?></p>

	<p><?php array_push($students, 'Jane Smith'); ?></p>
	<p><?php print_r($students); ?></p>

	<p><?php echo count($students); ?></p>

	<p><?php array_shift($students); ?></p>
	<p><?php print_r($students); ?></p>

	<p><?php echo count($students); ?></p>

</body>
</html>